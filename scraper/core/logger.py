import logging

DEFAULT_LOG_FORMAT = (
    '[%(levelname)s %(asctime)s.%(msecs)d] %(message)s'
)

logging.basicConfig(
    level=logging.INFO,
    format=DEFAULT_LOG_FORMAT,
    handlers=[logging.StreamHandler()]
)

log = logging.getLogger('scraper.tracer')
