from json import loads
from functools import cached_property
from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientError
from parsel import Selector
import jmespath
from scraper.core.logger import log
from scraper.settings import MIMTPROXY, MITMPROXY_ENABLED
from scraper.core.utils import get_trace_configs


class HTTPError(Exception):
    pass


class HTTPSession(ClientSession):

    def __init__(self):
        trace_configs = get_trace_configs()
        super(HTTPSession, self).__init__(
            trace_configs=trace_configs
        )

    async def http(
        self, url=None, verb=None, headers=None, params=None, data=None,
        redirect=True, timeout=None, as_json=False, encoding='utf-8',
        proxy=None, ssl=False
    ):
        # Use mitmproxy only when developing
        proxy = MIMTPROXY if MITMPROXY_ENABLED else proxy

        follow_fnxs = {
            'GET': self.get,
            'POST': self.post,
            'HEAD': self.head,
            'DELETE': self.delete,
            'OPTIONS': self.options
        }

        request_conf = HTTPRequestConfiguration(
            url=url, verb=verb, headers=headers, params=params, data=data,
            redirect=redirect, timeout=timeout, as_json=as_json, proxy=proxy,
            ssl=ssl
        )

        http_request = follow_fnxs[request_conf.verb](
            request_conf.url, **request_conf.as_dict()
        )

        try:
            async with http_request as raw_http_response:
                content = await raw_http_response.read()
                content = content.decode(encoding)
                return HTTPResponse(
                    content=content,
                    headers=raw_http_response.headers,
                    status=raw_http_response.status,
                    url=raw_http_response.url,
                    reason=raw_http_response.reason,
                    request_info=raw_http_response.request_info
                )
        except (ClientError, TimeoutError):
            raise HTTPError('An HTTP error occurred when performing the call')

    async def http_get(self, *args, **kwargs):
        kwargs['verb'] = 'GET'
        return await self.http(*args, **kwargs)

    async def http_post(self, *args, **kwargs):
        kwargs['verb'] = 'POST'
        return await self.http(*args, **kwargs)

    async def http_head(self, *args, **kwargs):
        kwargs['verb'] = 'HEAD'
        return await self.http(*args, **kwargs)

    async def http_delete(self, *args, **kwargs):
        kwargs['verb'] = 'DELETE'
        return await self.http(*args, **kwargs)

    async def http_options(self, *args, **kwargs):
        kwargs['verb'] = 'OPTIONS'
        return await self.http(*args, **kwargs)


class HTTPRequestConfiguration:
    '''
    Abstract the request settings
    '''
    def __init__(
        self, url=None, verb='GET', headers={}, params=None, data=None,
        redirect=True, timeout=None, as_json=None, proxy=None, ssl=False
    ):
        if not url:
            raise ValueError('URL is None, cannot perform HTTP call')
        self.url = url
        self.verb = verb
        self.headers = headers
        self.params = params
        self.data = data
        self.redirect = redirect
        self.timeout = timeout
        self.as_json = as_json
        self.proxy = proxy
        self.ssl = ssl

    def as_dict(self):
        params = {}

        if self.data:
            params['data'] = self.data

        if self.params:
            params['params'] = self.params

        params['headers'] = self.headers

        params['allow_redirects'] = self.redirect

        if self.timeout:
            params['timeout'] = self.timeout

        if self.as_json:
            params['json'] = self.as_json

        if self.proxy:
            params['proxy'] = self.proxy

        params['ssl'] = self.ssl

        return params


class HTTPResponse:
    '''
    Represents the HTTP response comming back from the
    server and allows to interact with the response in
    different ways providing a different toolset of mechanics.
    '''
    def __init__(
        self, content=None, headers=None, status=None,
        url=None, reason=None, request_info=None
    ):
        self.flow = None
        self.original_headers = None
        self.original_url = None

        self._content = content
        self._headers = headers
        self._status = status
        self._url = url
        self._reason = reason
        self._request_info = request_info

    @property
    def content(self):
        return self._content

    @property
    def headers(self):
        return self._headers

    @property
    def status(self):
        return self._status

    @property
    def url(self):
        return self._url

    @property
    def reason(self):
        return self._reason

    @property
    def session(self):
        return self._session

    @property
    def request_info(self):
        return self._request_info

    @cached_property
    def _selector(self):
        return Selector(text=self._content)

    @property
    def xpath(self):
        return self._selector.xpath

    @property
    def css(self):
        return self._selector.css

    @cached_property
    def json(self):
        try:
            return loads(self._content)
        except Exception as exc:
            log.debug('Could not parse content as json')
            raise exc

    def jmes(self, expression, default=None):
        result = jmespath.search(expression, self.json)
        return default if result is None else result
