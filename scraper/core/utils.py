from aiohttp import TraceConfig
from typing import Callable
from inspect import iscoroutinefunction
from .logger import log
from asyncio import sleep


async def _console_on_request_start(session, trace_config_ctx, params):
    headers = '\n'.join(
        f'\t {k} : {v}' for k, v in dict(params.headers).items()
    )
    log.info(f'{params.method} -> {params.url}')
    log.info(f'\n{headers}')


async def _console_on_request_end(session, trace_config_ctx, params):
    log.info(
        f'{params.response.status} ({params.response.reason})'
    )


def get_trace_configs():
    trace_config = TraceConfig()
    trace_config.on_request_start.append(_console_on_request_start)
    trace_config.on_request_end.append(_console_on_request_end)
    return [trace_config]


class Retry:

    ERR_MSG = 'Retrying function call due to {} error ({})'

    def __init__(
        self,
        excepts: tuple = None,
        max_retries: int = 3,
        fail_exception: Exception = None,
        retry_callback: Callable = None,
    ):
        self._excepts = tuple(excepts or [Exception])
        self._max_retries = max_retries
        self._fail_exception = fail_exception
        self._callback = retry_callback
        self._callback_is_coro = iscoroutinefunction(retry_callback)

    async def retry_callback(
        self, exception: Exception, retry: int, *args, **kwargs
    ):
        log.warning(self.ERR_MSG.format(exception, retry + 1))

        '''
        Apply callback either async or standard
        '''
        if self._callback and self._callback_is_coro:
            await self._callback(retry, *args, **kwargs)
        elif self._callback and not self._callback_is_coro:
            self._callback(retry, *args, **kwargs)
        else:
            await sleep(retry + 1)

    def __call__(self, fnx):

        async def decorated(*args: list, **kwargs: dict):
            last_exception = None

            for retry in range(self._max_retries):
                try:
                    return await fnx(*args, **kwargs)
                except self._excepts as ex:
                    last_exception = ex
                    await self.retry_callback(ex, retry, *args, **kwargs)

            raise (
                self._fail_exception if self._fail_exception
                else last_exception
            )

        return decorated
