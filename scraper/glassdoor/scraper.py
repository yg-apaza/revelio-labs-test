from json import loads
import re
from typing import List

from scraper.core.http import HTTPResponse, HTTPSession
from scraper.core.utils import Retry
from scraper.models.company import (
    Company, Review, ReviewEmployerResponse, Reviewer
)
from scraper.core.logger import log
from .settings import (
    MAX_REVIEWS_PAGE_NUMBER, URLS, CompanyNotFound,
    RetryInvalidRequest, ForbiddenByServer, CompanySlugNotFound
)


def _parse_company(
    company_id: str,
    basic_information: dict,
    overview_information: dict,
    reviews: List[Review]
):
    competitors = overview_information.get('competitors')
    ceo_name = overview_information.get('ceo_name')
    revenue = overview_information.get('Revenue:')
    size = basic_information.get('size')
    # Could not find any information about ticker on Glassdoor, more
    # research is needed here
    return Company.construct(
        company_id=company_id,
        company_name=basic_information.get('name'),
        company_website=overview_information.get('Website:'),
        company_revenue=revenue if 'Unknown' not in revenue else None,
        company_headquarters=basic_information.get('headquarters'),
        company_size=size if 'Unknown' not in size else None,
        company_ticker=None,
        company_ceo_name=ceo_name if ceo_name else None,
        company_competitors=competitors.split(',') if competitors else [],
        company_reviews=reviews
    )


def _parse_review(review: dict, additional_info: dict):
    location = review.get('location') or {}
    location_id = location.get('__ref')

    reviewer = _parse_reviewer(review, additional_info)
    employer_responses = [
        _parse_employer_response(e, additional_info)
        for e in review.get('employerResponses')
    ]

    return Review.construct(
        review_date_time=review.get('reviewDateTime'),
        review_location=additional_info.get(location_id, {}).get('name'),
        review_reviewer=reviewer,
        review_pros=review.get('pros'),
        review_cons=review.get('cons'),
        review_summary=review.get('summary'),
        review_advice=review.get('advice'),
        review_employer_responses=employer_responses
    )


def _parse_reviewer(review: dict, additional_info: dict):
    job_title = review.get('jobTitle') or {}
    job_title_id = job_title.get('__ref')

    return Reviewer.construct(
        reviewer_job_title=additional_info.get(job_title_id, {}).get('text'),
        reviewer_is_current_job=review.get('isCurrentJob'),
        reviewer_job_ending_year=review.get('jobEndingYear')
    )


def _parse_employer_response(employer_response: dict, additional_info: dict):
    response_id = employer_response.get('__ref')
    return ReviewEmployerResponse.construct(
        response_text=additional_info.get(response_id, {}).get('response'),
        response_job_title=additional_info.get(
            response_id, {}
        ).get('userJobTitle'),
    )


def _extract_basic_information(search_results: dict, company_id: str):
    basic_info = search_results.get(
        'apolloState', {}
    ).get(
        f'Employer:{company_id}', {}
    )
    if not basic_info:
        raise CompanyNotFound()
    return basic_info


def _extract_search_results(response: HTTPResponse):
    search_raw_json = response.xpath(
        '//script[contains(text(), "window.appCache")]/text()'
    ).get('')
    match = re.search(
        r'window.appCache\s*=\s*({.+});', search_raw_json, flags=re.DOTALL
    )
    return loads(match.group(1)) if match else {}


def _validate_response(response: HTTPResponse):
    """
    Check if we got a valid response from Glassdoor, if not
    raise a Retry exception
    """
    if response.status != 200:
        log.info('Retrying search request after invalid response')
        raise RetryInvalidRequest()


def _extract_overview_information(response: HTTPResponse):
    overview_selectors = response.xpath(
        '//div[@data-test="employerOverviewModule"]/ul/li'
    )
    overview_information = {
        item.xpath('./label/text()').get():
        item.xpath('./*[@data-test]/text()').get()
        for item in overview_selectors
    }

    competitors = response.xpath(
        'normalize-space(//p[@data-test="employerCompetitors"]/span)'
    ).get()

    ceo_name = response.xpath(
        '//div[@data-test="ceoNameAndPhoto"]/div[contains(@class, "ceoName")]'
        '/text()'
    ).get('').strip()

    return {
        **overview_information,
        'ceo_name': ceo_name,
        'competitors': competitors
    }


def _extract_number_of_review_pages(additional_info: dict):
    # Extract value from dictionary by prefix key
    root_query = additional_info.get('ROOT_QUERY') or {}
    prefix = 'employerReviews'
    result = [
        root_query.get(key, {}).get('numberOfPages')
        for key in root_query.keys()
        if key.startswith(prefix)
    ]

    # Minimum number of review pages is 1
    number_of_reviews_pages = result[0] if result else 1

    return number_of_reviews_pages


def _extract_reviews(response: HTTPResponse):
    """
    Extract the total number of reviews and all the reviews from current page
    """
    reviews_raw_json = response.xpath(
        './/script[contains(text(),"window.appCache")]/text()'
    ).get()
    reviews_match = re.search(
        r'"reviews":(.+)},"reviewLocations', reviews_raw_json, flags=re.DOTALL
    )
    reviews = loads(reviews_match.group(1)) if reviews_match else {}

    additional_info_match = re.search(
        r'"apolloState":(.+)};', reviews_raw_json
    )
    additional_info = (
        loads(additional_info_match.group(1)) if additional_info_match else {}
    )

    return (
        _extract_number_of_review_pages(additional_info),
        [_parse_review(review, additional_info) for review in reviews]
    )


@Retry(
    excepts=(RetryInvalidRequest, CompanyNotFound),
    max_retries=3,
    fail_exception=ForbiddenByServer()
)
async def _follow_search_company(ss: HTTPSession, company_id: str):
    """
    Get basic information from the search results about the company
    """
    # Search the company by id
    search_response = await ss.http_get(
        url=URLS['search'](company_id=company_id)
    )
    _validate_response(search_response)
    search_results = _extract_search_results(search_response)
    # Extract basic information
    return _extract_basic_information(search_results, company_id)


def _extract_company_slug(basic_information: dict):
    overview_url = basic_information.get('links', {}).get('overviewUrl', '')
    match = re.search(
        r'/Overview/Working-at-(.+)-EI_IE.+\.htm',
        overview_url
    )

    if match:
        return match.group(1)
    raise CompanySlugNotFound()


@Retry(
    excepts=(RetryInvalidRequest,),
    max_retries=3,
    fail_exception=ForbiddenByServer()
)
async def _follow_overview(
    ss: HTTPSession, company_slug: str, company_id: str
):
    """
    Get information from the overview page about the company
    """
    overview_response = await ss.http_get(
        url=URLS['overview'](
            company_slug=company_slug,
            company_id=company_id
        )
    )
    _validate_response(overview_response)
    return _extract_overview_information(overview_response)


async def _follow_reviews(
    ss: HTTPSession, company_id: str, company_slug: str
):
    reviews_page_response = await ss.http_get(
        url=URLS['reviews'](
            company_slug=company_slug,
            company_id=company_id,
            page_number=1
        )
    )

    total_review_pages, reviews = _extract_reviews(reviews_page_response)
    current_page = 2

    while (
        current_page <= MAX_REVIEWS_PAGE_NUMBER and
        current_page <= total_review_pages
    ):
        reviews_page_response = await ss.http_get(
            url=URLS['reviews'](
                company_slug=company_slug,
                company_id=company_id,
                page_number=current_page
            )
        )
        current_page_reviews = _extract_reviews(reviews_page_response)
        reviews.append(current_page_reviews)
        current_page = current_page + 1

    return reviews


async def get_company(ss: HTTPSession, company_id: str) -> Company:
    basic_information = await _follow_search_company(ss, company_id)
    company_slug = _extract_company_slug(basic_information)
    overview_information = await _follow_overview(ss, company_slug, company_id)

    reviews = await _follow_reviews(ss, company_id, company_slug)

    return _parse_company(
        company_id, basic_information, overview_information, reviews
    )


# Development test harness
if __name__ == '__main__':
    import asyncio
    from pprint import pprint
    from traceback import format_exc

    async def run():
        try:
            company_id = '40772'
            ss = HTTPSession()
            company = await get_company(ss, company_id)
            pprint(company.dict())
        except Exception:
            log.error(
                'Exception when extracting company with id '
                f'{company_id}. {format_exc()}'
            )
        finally:
            await ss.close()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
