_GLASSDOOR_BASE_URL = 'https://www.glassdoor.com'

# This will make the scraper to extract only the first pages of reviews
MAX_REVIEWS_PAGE_NUMBER = 5

URLS = {
    'search': (
        f'{_GLASSDOOR_BASE_URL}/Search/results.htm?'
        'keyword={company_id}'.format
    ),
    'overview': (
        f'{_GLASSDOOR_BASE_URL}/Overview/Working-at-'
        '{company_slug}-EI_IE{company_id}.htm'.format
    ),
    'reviews': (
        f'{_GLASSDOOR_BASE_URL}/Reviews/'
        '{company_slug}-Reviews-E{company_id}_P{page_number}.htm'.format
    )
}


class CompanyNotFound(Exception):
    """
    Raised when HTTP response was valid but company was
    not found on search results
    """
    pass


class CompanySlugNotFound(Exception):
    """
    Raised when company slug could not be found, making it impossible
    to continue extracting the reviews page
    """
    pass


class RetryInvalidRequest(Exception):
    """
    Raised when an invalid HTTP status was raised and a retry is needed
    """
    pass


class ForbiddenByServer(Exception):
    """
    Raised after reaching the maximum amount of retries
    to get a valid HTTP response
    """
    pass
