import asyncio
from aiofiles import open
from json import dumps
from traceback import format_exc
from time import time
from signal import SIGTERM
from functools import partial
from asyncio import Semaphore
from .core.http import HTTPSession
from .glassdoor.scraper import get_company
from .core.logger import log
from .settings import (
    FILE_STORAGE_RESULTS, MAX_WAIT_FOR_SHUTDOWN, DEFAULT_MAX_RUNNING_TASKS
)


async def wait_for_task_completion(consumer):
    """
    Wait for running tasks to complete
    """
    start_tms = time()
    while True:
        if (time() - start_tms) > MAX_WAIT_FOR_SHUTDOWN:
            log.info(
                'Waited for too long for completion, hard stop is '
                'going to happen, some running tasks might be lost'
            )
            consumer.stop()
            break
        elif consumer.number_running_tasks <= 0:
            log.info('All tasks are completed we can shutdown cleanly')
            consumer.stop()
            break
        else:
            log.info(
                'There are still active tasks, waiting for clean shutdown...'
            )
            await asyncio.sleep(2)


def exit_consumer(_result):
    log.info('Shutdown process completed for consumer')
    exit(0)


def gracefull_shutdown(loop, consumer):
    loop.create_task(
        wait_for_task_completion(consumer)
    ).add_done_callback(
        exit_consumer
    )


def start_consumer(loop, consumer):
    shutdown_process = partial(gracefull_shutdown, loop, consumer)
    loop.add_signal_handler(SIGTERM, shutdown_process)
    loop.run_until_complete(
        consumer.run()
    )


class Consumer():
    def __init__(self, max_running_tasks=DEFAULT_MAX_RUNNING_TASKS):
        self._queue = asyncio.Queue()
        self._stop = False
        self.semaphore = Semaphore(max_running_tasks)

    @property
    def number_running_tasks(self):
        return self._queue._unfinished_tasks

    def stop(self):
        self._stop = True

    async def feed(self, message):
        await self._queue.put(message)

    async def run(self):
        log.info('Running consumer')
        while not self._stop:
            message = await self._queue.get()
            asyncio.create_task(self._handle_message(message))

    async def _handle_message(self, message: str):
        async with self.semaphore:
            log.info(f'Extracting information of company {message}')
            try:
                ss = HTTPSession()
                company = await get_company(ss, message)
                # Append results to a text file
                async with open(FILE_STORAGE_RESULTS, 'a') as f:
                    await f.write(dumps(company.dict()))
            except Exception:
                log.error(
                    'Exception when extracting company with id '
                    f'{message}. {format_exc()}'
                )
            finally:
                self._queue.task_done()
                await ss.close()
