import asyncio
import os
from aiofiles import open

from .settings import FILE_STORAGE_RESULTS
from .consumer import Consumer, start_consumer


async def feed_consumer(consumer):
    async with open('companies.txt', mode='r') as f:
        async for company_id in f:
            await consumer.feed(company_id.strip())


if __name__ == '__main__':
    """
    Simulating consuming from a message broker.
    This will execute forever until the process get the SIGTERM signal.
    To stop do `make stop`
    """
    # Recreate results file
    if os.path.exists(FILE_STORAGE_RESULTS):
        os.remove(FILE_STORAGE_RESULTS)

    loop = asyncio.get_event_loop()
    consumer = Consumer()
    loop.create_task(feed_consumer(consumer))
    start_consumer(loop, consumer)
