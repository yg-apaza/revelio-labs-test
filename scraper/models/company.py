from pydantic import BaseModel
from typing import List


class Reviewer(BaseModel):
    reviewer_job_title: str
    reviewer_is_current_job: bool
    reviewer_job_ending_year: int


class ReviewEmployerResponse(BaseModel):
    response_text: str
    response_job_title: str


class Review(BaseModel):
    review_date_time: str
    review_location: str
    review_reviewer: Reviewer
    review_pros: str
    review_cons: str
    review_summary: str
    review_advice: str
    review_employer_responses: List[ReviewEmployerResponse]


class Company(BaseModel):
    company_id: str
    company_name: str
    company_website: str
    company_revenue: str
    company_headquarters: str
    company_size: str
    company_ticker: str
    company_ceo_name: str
    company_competitors: str
    company_reviews: List[Review]
