MIMTPROXY = 'http://mitmproxy:8080'

# Enable or disable mitmproxy
MITMPROXY_ENABLED = False

# Wait for a bit less than 30 seconds
MAX_WAIT_FOR_SHUTDOWN = 30 - 5

DEFAULT_MAX_RUNNING_TASKS = 5

FILE_STORAGE_RESULTS = 'result.txt'
