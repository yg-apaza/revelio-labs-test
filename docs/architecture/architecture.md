# Architecture

## Flow

I decided to use AsyncIO capabilities to concurrently perform the companies scraping. The number of concurrent extractions can be updated in the [settings file](../../scraper/settings.py) by changing the variable `DEFAULT_MAX_RUNNING_TASKS`. The flow is the following:

1. A consumer based on an AsyncIO Queue will start
2. We will add a concurrent task on the main event loop to feed the consumer with companies ids extracted from a file
3. The consumer will start handling each message (company id) and doing the scraping for each one.
4. A semaphore is used to allow only a certain number of concurrent scrapings.
5. The result of each message, is the company information in a JSON format, for now, this content is dumped in a simple file.

## Considerations for production environment

In a real production environment, we need to use a message broker and launch multiple consumers to consume from it. Also, we need to store the results on a database or some cloud storage service. Each consumer should be a pod in Kubernetes and we need to auto-scale them based on the number of messages we have in our message broker. This means that, if there are more messages to be processed more consumers (pods) should be launched.

## Graceful shutdown

When a consumer receives the `SIGTERM` signal it needs to gracefully shutdown its processes. For this to happen, I implemented a basic strategy that gives the consumer some additional time to complete pending tasks. If these tasks are not completed, then we need to reschedule these tasks (for the sake of this test, this part was not implemented).
