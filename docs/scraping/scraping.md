# Scraping analysis

Given a `company_id`, the scraper needs to perform the following steps to extract their information and reviews:

1. Search the company by using its id.
2. Get the company slug (identifier based on the company name).
3. Visit the `Overview` page and extract information.
4. Visit each `Review` page and extract the reviews.

## Step 1

Glassdoor search page allows us to find a company by putting the id as a `keyword`.

For example, lets search Facebook with id `40772`: [https://www.glassdoor.com/Search/results.htm?keyword=**40772**](https://www.glassdoor.com/Search/results.htm?keyword=40772) [[HTML sample](search_company.html)]

I found inside a script a JSON structure with the company's basic information. That information is also available in the HTML DOM but I prefer parsing a JSON because it's more organized and structured.

![Search company by id](search_company.png)

## Step 2

To get the `Overview` and `Reviews` URLs have this pattern: `/Overview/Working-at-<company_slug>-EI_IE<company_id>` and `}/Reviews/<company_slug>-Reviews-E<company_id>_P<page_number>.htm`. We already have the `Overview` URL on the previous JSON, from there we can get the company slug by using regex.

## Step 3

This step is pretty straightforward, just do a simple HTTP `GET` request to the `Overview` URL. I need to parse the HTML using XPath to extract the basic information, CEO name, and competitors.

## Step 4

We need to visit each page of the `Review` section, the reviews information is also available inside a script in the form of a JSON structure.

I put a limit of 5 pages at maximum to extract because I don't want to get banned by Glassdoor. This can be removed and the scraper will extract all the available review pages. Usually, I will rotate proxies in case of IP banning.

## Retry HTTP requests

I added a validation step for each request in case it fails (not a 200 response). If a request fails then it will raise an Exception which is a signal to Retry the request at least 3 times.