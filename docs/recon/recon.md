# Recon

I'm going to use Firefox browser dev tools to explore HTML DOM, network traffic, and debug Javascript code.

To perform additional analysis we can also use MITMproxy to check in detail the HTTP requests, but for now, the browser is enough.

## Finding 1.9 million companies

We can start by exploring the company search web page from Glassdoor: [https://www.glassdoor.com/Explore/browse-companies.htm](https://www.glassdoor.com/Explore/browse-companies.htm)

As of today (27th December), the results indicate that we can extract 364907 companies which are far from the 1.9 million employers stated in their [About page](https://www.glassdoor.com/about-us/).

![First view of search results](raw_search.png)

By inspecting the network traffic and filtering by domain (www.glassdoor.com) to get only the relevant HTTP requests, we can find an interesting endpoint:

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?minRating=3.5&maxRating=5&numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?minRating=3.5&maxRating=5&numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false)

![Network traffic](network_traffic.png)

Glassdoor uses an internal API (based on Elastic Search) to retrieve search results and we can take advantage of that endpoint (because no authorization is required) to get all possible companies' ids and names.

A first good step is to analyze all the possible filters, and check/remove some of them to get the maximum number of companies.

![API endpoint result traffic](endpoint.png)

### Minimum and maximum rating filters

Let's remove this filter so we can get the maximum amount of companies. From the API, we can infer that those filters are `minRating` and `maxRating`. Our new URL is:

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false)

![API endpoint with no rating filters](rating.png)

The number of results increased to 681290 companies and we are getting closer.

### Surge hiring filter

This filter shows only companies that are [active and aggressively hiring](https://www.glassdoor.com/blog/31-companies-with-hiring-surges-apply-now/).

We can't remove that filter, because it defaults to `false` in case it isn't present. Our scraper has to take this into account and extract companies with both `true` and `false` values for `surgeHiring` filter.

Until now, we only explored companies with `surgeHiring` in `false`, let's see the other companies with this URL:

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&**surgeHiring=true**
](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=true)

![API endpoint with surge hiring](hiring_surge.png)

We got an additional 4286 companies.

### Location filters

In the API we have `locationId` and `locationType` filters. But we need to return to the search web page to understand how does Glassdoor obtains these values.

1. As we type a search term in the Location input, it pops up some HTTP requests to another interesting endpoint. Given a search term, this endpoint can find every city and country with their `locationId` and `locationType`: [https://www.glassdoor.com/api-web/atlas/city/find.htm?matchCities=true&maxCitiesToReturn=10&matchStates=false&matchCountries=false&matchCitiesOnly=false&idProp=realId&term=united](https://www.glassdoor.com/api-web/atlas/city/find.htm?matchCities=true&maxCitiesToReturn=10&matchStates=false&matchCountries=false&matchCitiesOnly=false&idProp=realId&term=united)
![Search UI with location filter](location_network_traffic.png)

2. Let's try to search companies within the United States (`id` is `1` and `type` is `N`): [https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false&**locationId=1&locationType=N**](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=10&pageRequested=1&domain=glassdoor.com&surgeHiring=false&locationId=1&locationType=N)
![API endpoint with location filter](location.png)
Here we got 681290 results which is the same amount we obtained when doing the [rating filter removal](#minimum-and-maximum-rating-filters). We can infer that the default location is the United States and that is the reason we are not getting all the other companies around the world.

3. We can get the available companies in other countries, do quick math and validate if our search API will allow us to extract all the 1.9 million companies. Given a list of countries and their ids, I used the API to find the number of results, here are the results for each country:

| Country name   | locationId | locationType | # of results |
|----------------|------------|--------------|-------------:|
| United States  | 1          | N            | 681290       |
| Brazil         | 36         | N            | 191127       |
| India          | 115        | N            | 154611       |
| United Kingdom | 2          | N            | 115525       |
| Canada         | 3          | N            | 79819        |
| Germany        | 96         | N            | 47304        |
| Mexico         | 169        | N            | 45411        |
| Argentina      | 15         | N            | 27838        |
| Spain          | 219        | N            | 24150        |
| Italy          | 120        | N            | 23052        |
| Singapore      | 217        | N            | 22004        |
| Netherlands    | 178        | N            | 17428        |
|                |            | **TOTAL**    | **1429559**  |

We are getting closer to 1.9 million just by counting on these countries, we also need to add the number of companies with `surgeHiring` value in `true` and at the same time, we need to take into account that some companies have headquarters in multiple countries.

### Pages filters

The `numPerPage` filter will allow us to get more companies from a single request. I tried testing the limits of that filter, and the maximum possible value is `100`. So our scraper can extract more companies ids doing fewer requests.

The `pageRequested` value is the current page of our results and the limit for this value depends on the number of results. We can only retrieve the first `9999` results and if our query gives us more, then we won't be able to extract all of them. Given that the `numPerPage` filter will be set to `100`, then the maximum `pageRequest` value is `99`.

Passing those limits will make the API return an empty result array even if our query says that there are more available pages.

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=100&domain=glassdoor.com&surgeHiring=false&locationId=1&locationType=N](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?**numPerPage=100&pageRequested=100**&domain=glassdoor.com&surgeHiring=false&locationId=1&locationType=N)

![API endpoint with pages exceeded](pages.png)

### Other filters

Other useful filters are the `minEmployeeCount` and `maxEmployeeCount`, the predefined values can be `1 - 50`, `51 - 200`, `201 - 500`, `501 - 1000`, `1001 - 5000`, `5001 - 10000`, and `10000+ (maxEmployeeCount = -1)`.

We can also filter by `sectorIds` (industry), `jobTitle`, and `sgocIds` (job functions). Just something to keep in mind to make our queries return fewer than `9999` results.

Again, analyzing the search UI web page, we can see how to extract the value of these filters.

![HTTP request to get sector id](sector.png)

![HTTP request to get job title id](job_title.png)

![HTTP request with 3 filters](job_functions.png)

Also, the `domain` filter seems to not affect in any way to the search results, so we can safely remove it.

### Additional findings

We should be getting all the companies by now, even the ones with no rating. But Glassdoor also has companies that don't have any **additional information** (Reviews, Jobs, Salaries, Interviews, Benefits, Photos) apart from the basics and we are missing those companies in our search.

- To prove my point, see this company: [WSC Music, South Korea](https://www.glassdoor.com/Overview/Working-at-WSC-Music-EI_IE1327077.11,20.htm). That company doesn't have any additional information.
![Company with no information](company_empty.png)

Let's try to find that company with the little information we have (company size and location) using the query API: Location is South Korea with id `135` and the company has between 1 and 50 employees.

I searched in all the available pages of the results and could not find the `WSC Music` company.

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=1&surgeHiring=false&**locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50**](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=4&surgeHiring=false&locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50)

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=1&surgeHiring=true&**locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50**](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=4&surgeHiring=true&locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50)

- Now check this company: [GM Creative on South Korea](https://www.glassdoor.com/Overview/Working-at-GM-Creative-EI_IE1470653.11,22.htm). In contrast, this company has at least some `Benefits` information.
![Company with little information](company_little_information.png)

Let's try to find that company with the little information we have (company size and location) using the query API.

[https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=4&surgeHiring=false&**locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50**](https://www.glassdoor.com/seo/ajax/ugcSearch.htm?numPerPage=100&pageRequested=4&surgeHiring=false&locationId=135&locationType=N&minEmployeeCount=1&maxEmployeeCount=50)

We are able to find that company on page 4.

![HTTP request to get job title id](company_little_information_found.png)

> We can infer that the query API of Glassdoor won't return companies that don't have any additional information (Reviews, Jobs, Salaries, Interviews, Benefits, Photos), so we may be losing some information here. More research is needed to sort out this issue.

## Scraping flow

Assuming that we need to have an updated database of companies information, our scraper needs to consider the following points:

1. Have a preloaded list of locations and periodically maintain it: The scraper should retrieve the available locations with their ids and types using the corresponding API. This should be done each longer period (weekly or monthly maybe) to have an up-to-date list.

2. Have a preloaded list of industries, job titles, and job functions: It works in the same way as the locations. The period of extraction is also longer here.

3. Each short period of time (daily maybe), extract the companies' ids and names by using the query API (surgeHiring in false).
    - First, start by creating a query that filters only by country
    - If we found that there are more than 9999 results, update the query to find by cities, thus dividing our query.
    - If there are still more than 9999 results, update the query to filter by each company size.
    - If there are still more than 9999 results, then add another filter to extract by each industry, job title, and finally job functions if required.

4. Do the same but with the filter `surgeHiring` in true.

6. We should only store and update the information if the scraping was successful.