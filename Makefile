.DEFAULT_GOAL := help

help: ## Shows this help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m  %s\033[0m\n\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ 🚀 Getting started

build: ## Build docker image
	docker-compose build

up: ## Start the container
	docker-compose up

stop: ## Stop the container forcefully after 30 seconds
	docker-compose stop -t 30 revelio-scraper

##@ 🛠  Development and debugging tools

mitmproxy: ## Run mitmproxy to track network traffic
	docker run --rm -it --name mitmproxy \
		--network revelio-scraper-net \
		mitmproxy/mitmproxy mitmproxy

python: ## Run for development purposes a single glassdoor scraper
	docker-compose run --rm revelio-scraper python -m scraper.glassdoor.scraper

flake8: ## Run flake8 linter
	docker-compose run --rm revelio-scraper flake8 .