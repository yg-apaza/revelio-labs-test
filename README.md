# Revelio Labs - Web Scraping Engineer

Simple scraper to concurrently extract company information from Glassdoor

## Usage

The application will start a consumer that will be feeded with companies ids extracted from a file, thus, simulating a production environment with a message broker. Execute the following commands to build and start the application:

```
make build
make up
```

To stop the consumer run:

```
make stop
```

This will stop the consumer after 30 seconds and giving time to gracefully shutdown pending tasks. The results of the extraction will be stored in a file `results.txt`.

## Development

During development, you can inspect network traffic from the scraper by starting MITMProxy and then running your application using the development test harness script. First, update the value of `MITMPROXY_ENABLED` to `True` on the file `scraper/settings.py`, then run the following commands:

```
make mitmproxy
make python
```

To ensure that the code follows PEP8 standard, run:

```
make flake8
```

## Documentation

- [Recon](docs/recon/recon.md): Findings on retrieving company list pairs of `id` and `name` pairs from Glasdoor.

- [Scraping analysis](docs/scraping/scraping.md): Analysis of company information scraping from Glassdoor given an `id`.

- [Architecture](docs/architecture/architecture.md): Simple architecture description to concurrently perform scraping on Glassdoor.
